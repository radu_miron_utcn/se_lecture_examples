package edu.tucn.lectureexamples.lecture2;

/**
 * @author Radu Miron
 * @version 1
 * @since WS 11.7
 */
public class Main {
    public static void main(String[] args) {
        Car car1 = new Car("Dacia", "Logan", 1600);
        Car car2 = new Car("Renault", "Symbol", 1500);

        System.out.println(car1.toString());
        System.out.println(car2.toString());

        System.out.println("The " + car1.toString() + " goes for " + car1.go() + " km");


        System.out.println(Car.STANDARD_NUMBER_OF_WHEELS);
    }
}
