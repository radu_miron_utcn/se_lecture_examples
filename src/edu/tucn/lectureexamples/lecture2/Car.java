package edu.tucn.lectureexamples.lecture2;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    private String brand;
    private String model;
    private int engineSize;
    public static final int STANDARD_NUMBER_OF_WHEELS = 4;

    public Car(String brand, String model, int engineSize) {
        this.brand = brand;
        this.model = model;
        this.engineSize = engineSize;
    }

    private void start() {
        System.out.println(this.toString() + " starts");
    }

    public int go() {
        start();
        return new Random().nextInt(50);
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", engineSize=" + engineSize +
                '}';
    }
}
