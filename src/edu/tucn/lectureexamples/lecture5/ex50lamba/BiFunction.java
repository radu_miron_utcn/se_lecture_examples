package edu.tucn.lectureexamples.lecture5.ex50lamba;

/**
 * @author Radu Miron
 * @version 1
 */
@FunctionalInterface
public interface BiFunction {
    int function(int a, int b);
}
