package edu.tucn.lectureexamples.lecture5.ex50lamba;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        BiFunction function1 = (a, b) -> a + b;
        Test.calculateValue(function1, 10, 1);
        Test.calculateValue((a, b) -> a - b, 10, 1);
    }
}
