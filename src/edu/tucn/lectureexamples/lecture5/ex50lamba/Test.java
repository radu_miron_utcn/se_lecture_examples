package edu.tucn.lectureexamples.lecture5.ex50lamba;

/**
 * @author Radu Miron
 * @version 1
 */
public class Test {
    static void calculateValue(BiFunction biFunction, int a, int b) {
        System.out.println("r = " + biFunction.function(a, b));
    }
}
