package edu.tucn.lectureexamples.lecture5.ex34interfacewithlambda;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    public Window() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        int x = new Random().nextInt(1500);
        int y = new Random().nextInt(800);
        this.setBounds(x, y, 100, 100);
        JButton button = new JButton("Click!");
        this.add(button);
        this.setVisible(true);

        // x -> f(x)
        // x -> x + 1

        ActionListener buttonHandler = actionEvent -> new Window();

        button.addActionListener(e -> {
            Window window = new Window();
            window.setTitle(e.getActionCommand());
        });
    }
}
