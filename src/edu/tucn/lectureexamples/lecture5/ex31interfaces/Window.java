package edu.tucn.lectureexamples.lecture5.ex31interfaces;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame implements ActionListener {
    public Window() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        int x = new Random().nextInt(1500);
        int y = new Random().nextInt(800);
        this.setBounds(x, y, 100, 100);
        JButton button = new JButton("Click!");
        this.add(button);
        this.setVisible(true);
        button.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        new Window();
    }
}
