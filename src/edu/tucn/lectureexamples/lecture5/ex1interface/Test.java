package edu.tucn.lectureexamples.lecture5.ex1interface;

/**
 * @author Radu Miron
 * @version 1
 */
public class Test {
    public static void main(String[] args) {
        System.out.println(TestInterface.ATTRIBUTE);
        // interface variables are constants
//        TestInterface.ATTRIBUTE = 25;
    }
}
