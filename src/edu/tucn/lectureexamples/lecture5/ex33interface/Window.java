package edu.tucn.lectureexamples.lecture5.ex33interface;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    public Window() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        int x = new Random().nextInt(1500);
        int y = new Random().nextInt(800);
        this.setBounds(x, y, 100, 100);
        JButton button = new JButton("Click!");
        this.add(button);
        this.setVisible(true);

        ActionListener buttonHandler = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                new Window();
            }
        };

        button.addActionListener(buttonHandler);
    }
}
