package edu.tucn.lectureexamples.lecture5.ex3interface;

import javax.swing.*;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    public Window() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        int x = new Random().nextInt(1500);
        int y = new Random().nextInt(800);
        this.setBounds(x, y, 100, 100);
        JButton button = new JButton("Click!");
        this.add(button);
        this.setVisible(true);
        ButtonHandler buttonHandler = new ButtonHandler();
        button.addActionListener(buttonHandler);
    }
}
