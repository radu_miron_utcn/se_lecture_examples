package edu.tucn.lectureexamples.lecture5.ex3interface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Radu Miron
 * @version 1
 */
public class ButtonHandler implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        new Window();
    }
}
