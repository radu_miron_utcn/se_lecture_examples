package edu.tucn.lectureexamples.lecture5.ex2interface;

/**
 * @author Radu Miron
 * @version 1
 */
public class Class1 implements Interface1, Interface2 {
    @Override
    public void met1() {
        System.out.println("method 1");
    }

    @Override
    public void met2() {
        System.out.println("method 2");
    }

    @Override
    public void met3() {
        System.out.println("method 3");
    }
}
