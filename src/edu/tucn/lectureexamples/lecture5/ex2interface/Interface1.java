package edu.tucn.lectureexamples.lecture5.ex2interface;

/**
 * @author Radu Miron
 * @version 1
 */
public interface Interface1 {
    void met1();
    void met2();
}
