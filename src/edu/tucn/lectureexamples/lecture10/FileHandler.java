package edu.tucn.lectureexamples.lecture10;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileHandler {
    public static void main(String[] args) throws IOException {
        generateFile("test3.txt");

        long t1 = System.currentTimeMillis();
        System.out.println(parseFile("test3.txt"));
        long t2 = System.currentTimeMillis();

        System.out.println(t2-t1);
    }

    public static void generateFile(String fileName) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(fileName)) {
            for (int i = 0; i < 10000000; i++) {
                out.println(new Random().nextInt(Integer.MAX_VALUE));
            }

            out.flush();
        }
    }

    public static String parseFile(String fileName) throws IOException {
        Map<Integer, Integer> occurrences = new HashMap<>();

        Pattern pattern = Pattern.compile("\\d");
        Files.lines(Paths.get(fileName)).forEach(l -> {
            Matcher matcher = pattern.matcher(l);

            while (matcher.find()) {
                int digit = Integer.parseInt(matcher.group(0));
                int numOfOccurrences = occurrences.get(digit) == null ? 1 : occurrences.get(digit) + 1;

                occurrences.put(digit, numOfOccurrences);
            }
        });

        StringBuilder builder = new StringBuilder();
        builder.append(fileName).append("\n");
        occurrences.entrySet().forEach(e -> {
            builder.append(e.getKey()).append(" -> ").append(e.getValue()).append("\n");
        });
        builder.append("\n");

        return builder.toString();
    }

    public static Map<Integer, Integer> parseFileV2(String fileName) throws IOException {
        Map<Integer, Integer> occurrences = new HashMap<>();

        Pattern pattern = Pattern.compile("\\d");
        Files.lines(Paths.get(fileName)).forEach(l -> {
            Matcher matcher = pattern.matcher(l);

            while (matcher.find()) {
                int digit = Integer.parseInt(matcher.group(0));
                int numOfOccurrences = occurrences.get(digit) == null ? 1 : occurrences.get(digit) + 1;

                occurrences.put(digit, numOfOccurrences);
            }
        });

        return occurrences;
    }

}
