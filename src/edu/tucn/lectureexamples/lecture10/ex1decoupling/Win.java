package edu.tucn.lectureexamples.lecture10.ex1decoupling;

import edu.tucn.lectureexamples.lecture10.FileHandler;

import javax.swing.*;
import java.io.IOException;
import java.util.Map;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    Win() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        setSize(500, 700);

        JTextField fileTF = new JTextField();
        fileTF.setBounds(20, 40, 460, 20);

        JTextArea results = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(results);
        scrollPane.setBounds(20, 80, 460, 400);

        JButton button = new JButton("Process File");
        button.setBounds(20, 500, 460, 20);

        JButton button2 = new JButton("Test Message");
        button2.setBounds(20, 540, 460, 20);
        button2.addActionListener(e -> System.out.println(Thread.currentThread().getName()));

        // v1
//        button.addActionListener(e -> {
//            new Thread(() -> {
//                try {
//                    results.append(FileHandler.parseFile(fileTF.getText().trim()));
//                } catch (IOException ex) {
//                    ex.printStackTrace();
//                }
//            }).start();
//        });

        // v2
        button.addActionListener(ae -> {
            new Thread(() -> {
                try {
                    String fileName = fileTF.getText().trim();
                    Map<Integer, Integer> occurrences = FileHandler.parseFileV2(fileName);

                    synchronized (results) {
                        results.append(fileName);
                        results.append("\n");
                        occurrences.entrySet().forEach(e -> {
                            results.append(e.getKey() + "");
                            results.append(" -> ");
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException ex) {
                            }
                            results.append(e.getValue() + "");
                            results.append("\n");
                        });
                        results.append("\n");
                    }

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }).start();
        });

        add(fileTF);
        add(scrollPane);
        add(button);
        add(button2);
        setVisible(true);
    }
}
