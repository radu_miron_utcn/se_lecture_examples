package edu.tucn.lectureexamples.lecture7.ex3serialization.ver1;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    private PersonRepository personRepository;

    public Window(PersonRepository personRepository) {
        this.personRepository = personRepository;
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(500, 500);
        this.setTitle("Person Manager");

        JLabel idNumLabel = new JLabel("ID Number");
        idNumLabel.setBounds(20, 20, 290, 20);

        JLabel firstNameLabel = new JLabel("First Name");
        firstNameLabel.setBounds(20, 70, 290, 20);

        JLabel lastNameLabel = new JLabel("Last Name");
        lastNameLabel.setBounds(20, 120, 290, 20);

        JTextField idNumTF = new JTextField();
        idNumTF.setBounds(140, 20, 290, 20);

        JTextField firstNameTF = new JTextField();
        firstNameTF.setBounds(140, 70, 290, 20);

        JTextField lastNameTF = new JTextField();
        lastNameTF.setBounds(140, 120, 290, 20);

        JTextArea details = new JTextArea();
        details.setBounds(20, 170, 420, 200);

        JButton create = new JButton("Create");
        create.setBounds(20, 400, 100, 20);
        // todo: validate the inputs
        create.addActionListener(e -> {
            personRepository.create(
                    new PersonDTO(idNumTF.getText(), firstNameTF.getText(), lastNameTF.getText()));
            idNumTF.setText("");
            firstNameTF.setText("");
            lastNameTF.setText("");
            details.setText("Person created");
        });

        JButton read = new JButton("Find");
        read.setBounds(130, 400, 100, 20);
        read.addActionListener(e -> details.setText(personRepository.read(idNumTF.getText()).toString()));

        JButton update = new JButton("Update");
        update.setBounds(240, 400, 100, 20);
        update.addActionListener(e -> {
            System.out.println("update"); // //todo: call the repo method
        });

        JButton delete = new JButton("Delete");
        delete.setBounds(350, 400, 100, 20);
        delete.addActionListener(e -> {
            System.out.println("delete"); // todo: call the repo method
        });

        this.add(idNumLabel);
        this.add(firstNameLabel);
        this.add(lastNameLabel);
        this.add(idNumTF);
        this.add(firstNameTF);
        this.add(lastNameTF);
        this.add(details);
        this.add(create);
        this.add(read);
        this.add(update);
        this.add(delete);
        this.setVisible(true);
    }
}
