package edu.tucn.lectureexamples.lecture7.ex3serialization.ver1;

/**
 * @author Radu Miron
 * @version 1
 */
public class PersonRepository {
    private PersonDTO[] persons = new PersonDTO[500];
    private int counter = 0;

    public void create(PersonDTO personDTO) {
        persons[counter++] = personDTO;
    }

    public PersonDTO read(String idNum) {
        for (PersonDTO person : persons) {
            if (idNum.equals(person.getIdNum())) {
                return person;
            }
        }

        return null;
    }

    public void update(PersonDTO personDTO) {
        //todo: implement
    }

    public void delete(String idNum) {
        // todo: implement
    }
}
