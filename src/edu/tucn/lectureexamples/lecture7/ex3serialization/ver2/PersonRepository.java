package edu.tucn.lectureexamples.lecture7.ex3serialization.ver2;

/**
 * @author Radu Miron
 * @version 1
 */
public interface PersonRepository {

    void create(PersonDTO personDTO);

    PersonDTO read(String idNum);

    void update(PersonDTO personDTO);

    void delete(String idNum);
}
