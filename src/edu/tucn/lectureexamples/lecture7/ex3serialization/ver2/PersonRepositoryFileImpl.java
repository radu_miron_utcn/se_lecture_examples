package edu.tucn.lectureexamples.lecture7.ex3serialization.ver2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

/**
 * @author Radu Miron
 * @version 1
 */
public class PersonRepositoryFileImpl implements PersonRepository {
    private static final String REPO_PATH = "/tmp/_repository";

    public PersonRepositoryFileImpl() {
        File repoDirectory = new File(REPO_PATH);

        if (!repoDirectory.exists() || repoDirectory.isFile()) {
            repoDirectory.mkdir();
        }
    }

    public void create(PersonDTO personDTO) {
        File personFile = new File(new File(REPO_PATH), personDTO.getIdNum() + ".txt");
        try (FileWriter fileWriter = new FileWriter(personFile)) {
            fileWriter.write(personDTO.toString());
            fileWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
            // todo: handle the exception in a nicer manner (show it in a popup maybe)
        }
    }

    public PersonDTO read(String idNum) {
        File personFile = new File(new File(REPO_PATH), idNum + ".txt");

        if (personFile.exists()) {
            try {
                String personAsString = Files.lines(personFile.toPath())
                        .findFirst()
                        .orElse("");

                int startId = personAsString.indexOf("idNum='") + 7;
                int endId = personAsString.indexOf("', f");
                String parsedIdNum = personAsString.substring(startId, endId);
                // todo: parse the remaining fields (firstName, lastName)

                return new PersonDTO(parsedIdNum, "<mock>", "<mock>");
            } catch (IOException e) {
                e.printStackTrace();
                // todo: handle the exception in a nicer manner (show it in a popup maybe)
            }

        }

        return null;
    }

    public void update(PersonDTO personDTO) {
        //todo: implement
    }

    public void delete(String idNum) {
        // todo: implement
    }
}
