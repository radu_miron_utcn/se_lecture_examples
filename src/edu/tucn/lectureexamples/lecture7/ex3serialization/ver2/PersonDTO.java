package edu.tucn.lectureexamples.lecture7.ex3serialization.ver2;

/**
 * @author Radu Miron
 * @version 1
 */
public class PersonDTO {
    private String idNum;
    private String firstName;
    private String lastName;

    public PersonDTO(String idNum, String firstName, String lastName) {
        this.idNum = idNum;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getIdNum() {
        return idNum;
    }

    public void setIdNum(String idNum) {
        this.idNum = idNum;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "PersonDTO{" +
                "idNum='" + idNum + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
