package edu.tucn.lectureexamples.lecture11.util;

/**
 * @author Radu Miron
 * @version 1
 */
public class ActivityUtils {

    public static final String FILE_NAME = "testfiles/prod-cons-file.txt";

    public static void doActivity(String activityName, int time) {
        String currentThreadName = Thread.currentThread().getName();

        System.out.println(String.format("[%s] Start executing %s",  currentThreadName, activityName));
        try {
            Thread.sleep(time * 1000);
        } catch (InterruptedException e) {
        }
        System.out.println(String.format("[%s] Finished %s", currentThreadName, activityName));
    }

    public static synchronized void doCriticalActivity(String activityName, int time) {
        String currentThreadName = Thread.currentThread().getName();

        System.out.println(String.format("[%s] Start executing %s",  currentThreadName, activityName));
        try {
            Thread.sleep(time * 1000);
        } catch (InterruptedException e) {
        }
        System.out.println(String.format("[%s] Finished %s", currentThreadName, activityName));
    }
}
