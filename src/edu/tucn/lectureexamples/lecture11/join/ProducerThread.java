package edu.tucn.lectureexamples.lecture11.join;

import edu.tucn.lectureexamples.lecture10.FileHandler;
import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

import java.io.FileNotFoundException;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class ProducerThread extends Thread {

    @Override
    public void run() {
        try {
            System.out.println("Start generating the file");
            FileHandler.generateFile(ActivityUtils.FILE_NAME);
            System.out.println("Producer finished generating the file");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
