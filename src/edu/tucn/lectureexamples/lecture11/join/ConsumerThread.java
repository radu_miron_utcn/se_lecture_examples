package edu.tucn.lectureexamples.lecture11.join;

import edu.tucn.lectureexamples.lecture10.FileHandler;
import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ConsumerThread extends Thread {
    private ProducerThread producerThread;

    public ConsumerThread(ProducerThread producerThread) {
        this.producerThread = producerThread;
    }

    @Override
    public void run() {
        try {
            producerThread.join();

            System.out.println("Consumer got notified");

            System.out.println(FileHandler.parseFile(ActivityUtils.FILE_NAME));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
