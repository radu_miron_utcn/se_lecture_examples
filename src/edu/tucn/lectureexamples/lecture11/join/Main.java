package edu.tucn.lectureexamples.lecture11.join;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        ProducerThread producerThread = new ProducerThread();
        producerThread.start();
        new ConsumerThread(producerThread).start();
    }
}
