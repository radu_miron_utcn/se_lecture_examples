package edu.tucn.lectureexamples.lecture11.waitnotifysemaphore;

import edu.tucn.lectureexamples.lecture10.FileHandler;
import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

import java.io.FileNotFoundException;
import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class ProducerThread extends Thread {
    private Object object;

    public ProducerThread(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        try {
            System.out.println("Start generating the file");
            FileHandler.generateFile(ActivityUtils.FILE_NAME);
            System.out.println("Producer finished generating the file");

            ((Semaphore)object).release();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
