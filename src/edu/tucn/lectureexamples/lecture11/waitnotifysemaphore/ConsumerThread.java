package edu.tucn.lectureexamples.lecture11.waitnotifysemaphore;

import edu.tucn.lectureexamples.lecture10.FileHandler;
import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class ConsumerThread extends Thread {
    private Semaphore semaphore;

    public ConsumerThread(Semaphore semaphore) {
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }

        try {
            semaphore.acquire();

            System.out.println("Consumer got notified");

            System.out.println(FileHandler.parseFile(ActivityUtils.FILE_NAME));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
