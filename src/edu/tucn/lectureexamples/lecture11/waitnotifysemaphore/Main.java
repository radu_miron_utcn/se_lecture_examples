package edu.tucn.lectureexamples.lecture11.waitnotifysemaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(0);
        new ProducerThread(semaphore).start();
        new ConsumerThread(semaphore).start();
    }
}
