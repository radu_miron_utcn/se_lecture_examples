package edu.tucn.lectureexamples.lecture11.waitnotify;

import edu.tucn.lectureexamples.lecture10.FileHandler;
import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class ConsumerThread extends Thread {
    private Object object;

    public ConsumerThread(Object object) {
        this.object = object;
    }

    @Override
    public void run() {
        // comment the sleep if you don't want you consumer to get stuck
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }


        synchronized (object) {
            try {
                object.wait();

                System.out.println("Consumer got notified");

                System.out.println(FileHandler.parseFile(ActivityUtils.FILE_NAME));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
