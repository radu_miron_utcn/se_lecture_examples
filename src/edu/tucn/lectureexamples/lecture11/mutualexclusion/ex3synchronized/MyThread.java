package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex3synchronized;

import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {

    public MyThread(String name) {
        this.setName(name);
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        synchronized (MyThread.class) {
            ActivityUtils.doActivity("A2", 5);
        }

        ActivityUtils.doActivity("A3", 1);
    }
}
