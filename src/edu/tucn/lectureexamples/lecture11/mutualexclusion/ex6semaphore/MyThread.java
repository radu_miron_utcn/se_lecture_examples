package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex6semaphore;

import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private Semaphore semaphore;

    public MyThread(String name, Semaphore semaphore) {
        this.setName(name);
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        try {
            semaphore.acquire();
            ActivityUtils.doActivity("A2", 5);
            semaphore.release();
        } catch (InterruptedException e) {
        }

        ActivityUtils.doActivity("A3", 1);
    }
}
