package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex6semaphore;

import java.util.concurrent.Semaphore;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        // we can have 2 out of the 3 threads entering the critical region
        Semaphore semaphore = new Semaphore(2);
        new MyThread("T1", semaphore).start();
        new MyThread("T2", semaphore).start();
        new MyThread("T2", semaphore).start();
    }
}
