package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex4synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        new MyThread("T1").start();
        new MyThread("T2").start();
    }
}
