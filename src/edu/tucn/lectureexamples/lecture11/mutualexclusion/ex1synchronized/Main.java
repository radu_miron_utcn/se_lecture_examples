package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex1synchronized;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Object lock = new Object();
        new MyThread("T1", lock).start();
        new MyThread("T2", lock).start();

        // wrong way of using the monitor; it has to be the same object for both threads
//        Object lock = new Object();
//        Object lock1 = new Object();
//        new MyThread("T1", lock).start();
//        new MyThread("T2", lock1).start();
    }
}
