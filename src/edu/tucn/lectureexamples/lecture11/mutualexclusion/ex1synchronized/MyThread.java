package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex1synchronized;

import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private Object lock;

    public MyThread(String name, Object lock) {
        this.setName(name);
        this.lock = lock;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        synchronized (lock) {
            ActivityUtils.doActivity("A2", 5);
        }

        ActivityUtils.doActivity("A3", 1);
    }
}
