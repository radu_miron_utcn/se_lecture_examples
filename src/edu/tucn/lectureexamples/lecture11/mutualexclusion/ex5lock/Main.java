package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex5lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Lock lock = new ReentrantLock();
        new MyThread("T1", lock).start();
        new MyThread("T2", lock).start();
    }
}
