package edu.tucn.lectureexamples.lecture11.mutualexclusion.ex5lock;

import edu.tucn.lectureexamples.lecture11.util.ActivityUtils;

import java.util.concurrent.locks.Lock;

/**
 * @author Radu Miron
 * @version 1
 */
public class MyThread extends Thread {
    private Lock lock;

    public MyThread(String name, Lock lock) {
        this.setName(name);
        this.lock = lock;
    }

    @Override
    public void run() {
        ActivityUtils.doActivity("A1", 1);

        lock.lock();
        ActivityUtils.doActivity("A2", 5);
        lock.unlock();

        ActivityUtils.doActivity("A3", 1);
    }
}
