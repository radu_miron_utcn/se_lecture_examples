package edu.tucn.lectureexamples.lecture8.game;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame implements KeyListener {
    List<Enemy> enemies;
    private MainCharacter mainCharacter;

    public Win(MainCharacter mainCharacter, List<Enemy> enemies) {
        setSize(500, 500);
        setTitle("My Game");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(null);
        this.mainCharacter = mainCharacter;
        this.mainCharacter.setBounds(230, 430, 40, 20);

        add(this.mainCharacter);
        this.addKeyListener(this);

        this.enemies = enemies;
        for (Enemy enemy : enemies) {
            add(enemy);
        }

        setVisible(true);
    }


    @Override
    public void keyTyped(KeyEvent keyEvent) {
        if (keyEvent.getKeyChar() == 'a' && this.mainCharacter.getX() > 0) {
            this.mainCharacter.setLocation(this.mainCharacter.getX() - 5, this.mainCharacter.getY());
        } else if (keyEvent.getKeyChar() == 'd' && this.mainCharacter.getX() < 460) {
            this.mainCharacter.setLocation(this.mainCharacter.getX() + 5, this.mainCharacter.getY());
        }
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
    }
}
