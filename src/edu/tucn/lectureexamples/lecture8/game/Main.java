package edu.tucn.lectureexamples.lecture8.game;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        List<Enemy> enemies = new ArrayList<>();

        for (int i = 0; i < 5; i++) {
            Enemy enemy = new Enemy();
            enemy.setBounds(i * 100 + 30, 0, 20, 20);
            enemies.add(enemy);
        }

        MainCharacter mainCharacter = new MainCharacter();
        new Win(mainCharacter, enemies);


        // todo: add different speed for each enemy
        // todo: detect the collisions and end the game if they happen
        // todo: use you imagination to implement a proper game (different levels, score, etc.)
        while (true) {
            for (Enemy enemy : enemies) {
                enemy.setLocation(enemy.getX(), enemy.getY() + 10);
                enemy.repaint();
            }

            Thread.sleep(200);
        }
    }
}
