package edu.tucn.lectureexamples.lecture8.ex1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame implements ActionListener {
    private JButton b1;
    private JButton b2;

    public Win() {
        setLayout(new GridLayout(1,2));
        b1 = new JButton("Press 1");
        b2 = new JButton("Press 2");

        b1.addActionListener(this);
        b2.addActionListener(this);

        setSize(200, 20);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        add(b1);
        add(b2);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        new Win();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource().equals(b1)) {
            System.out.println("do something");
        } else if (actionEvent.getSource().equals(b2)) {
            System.out.println("do something else");
        }

        System.out.println(actionEvent.getActionCommand());
    }
}
