package edu.tucn.lectureexamples.lecture9.ex5singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {

        new Thread(()->{
            Singleton singleton = Singleton.getInstance();
            System.out.println(singleton);
        }).start();

        new Thread(()->{
            Singleton singleton = Singleton.getInstance();
            System.out.println(singleton);
        }).start();

        Singleton singleton1 = Singleton.getInstance();
        System.out.println(singleton1);

    }
}
