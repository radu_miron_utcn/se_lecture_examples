package edu.tucn.lectureexamples.lecture9.ex5singleton;

/**
 * @author Radu Miron
 * @version 1
 */
public class Singleton {

    private static volatile Singleton instance;

    private Singleton() {
    }

    public static synchronized Singleton getInstance() {
        if (instance == null) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
            }

            instance = new Singleton();
        }

        return instance;
    }

    public void test() {
        System.out.println("this is a method");
    }

    public void test1() {
        System.out.println("this is another method");
    }
}
