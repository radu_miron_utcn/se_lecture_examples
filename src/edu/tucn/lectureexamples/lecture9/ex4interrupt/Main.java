package edu.tucn.lectureexamples.lecture9.ex4interrupt;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        SlowExec slowExec = new SlowExec();
        Intrerruptor intrerruptor = new Intrerruptor(slowExec);

        slowExec.start();
        intrerruptor.start();

        //we can do the same thing (interrupt the sleep()) with the main thread
//        try {
//            Thread.sleep(5000); // 5 sec
//        } catch (InterruptedException e) {
//        }
//        slowExec.interrupt();
    }
}

class SlowExec extends Thread {
    @Override
    public void run() {
        try {
            System.out.println("do something");
            Thread.sleep(1000 * 60 * 60 * 24); // 1 day
        } catch (InterruptedException e) {
            System.out.println("I've been interrupted");
        }
    }
}

class Intrerruptor extends Thread {
    private SlowExec slowExec;

    public Intrerruptor(SlowExec slowExec) {
        this.slowExec = slowExec;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(5000); // 5 sec
        } catch (InterruptedException e) {
        }
        slowExec.interrupt();
    }
}