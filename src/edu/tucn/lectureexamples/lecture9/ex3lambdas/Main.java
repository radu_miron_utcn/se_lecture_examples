package edu.tucn.lectureexamples.lecture9.ex3lambdas;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        for (int k = 0; k < 3; k++) {
            new Thread(() -> {
                for (int i = 0; i < 10; i++) {
                    System.out.println(String.format("Thread %s - message number %d", Thread.currentThread().getName(), i));

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                    }
                }
            }).start();
        }

        System.out.println("this is the thread: " + Thread.currentThread().getName());
    }
}
