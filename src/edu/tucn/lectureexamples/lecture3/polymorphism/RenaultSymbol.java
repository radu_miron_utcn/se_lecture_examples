package edu.tucn.lectureexamples.lecture3.polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class RenaultSymbol extends Car {
    public RenaultSymbol(int year) {
        super(RenaultSymbol.class.getSimpleName(), year);
    }

    @Override
    public void moveForward() {
        super.moveForward();
        System.out.println("faster");
    }
}
