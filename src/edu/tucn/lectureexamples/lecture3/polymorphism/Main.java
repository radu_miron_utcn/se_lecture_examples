package edu.tucn.lectureexamples.lecture3.polymorphism;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Choose your car: 1. Dacia Logan, 2. Renault Symbol");
        int choice = scanner.nextInt();

        Car car = null;

        switch (choice) {
            case 1:
                car = new DaciaLogan(2020);
                break;
            case 2:
                car = new RenaultSymbol(2021);
                break;
            default:
                System.err.println("Invalid choice!");
                System.exit(1);
        }

        car.start();
        car.moveForward();
        car.stop();
    }
}
//    DaciaLogan starts
//    DaciaLogan moves forward fast
//    DaciaLogan stops

//    RenaultSymbol starts
//    RenaultSymbol moves forward faster
//    RenaultSymbol stops