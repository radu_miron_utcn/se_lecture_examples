package edu.tucn.lectureexamples.lecture3.polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public abstract class Car {
    private String name;
    private int year;

    public Car(String name, int year) {
        this.year = year;
        this.name = name;
    }

    public void start() {
        System.out.println(this.name + " starts");
    }

    public void moveForward() {
        System.out.print(this.name + " moves forward ");
    }

    public void stop() {
        System.out.println(this.name + " stops");
    }

    public String getName() {
        return name;
    }
}
