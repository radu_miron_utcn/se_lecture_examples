package edu.tucn.lectureexamples.lecture3.polymorphism;

/**
 * @author Radu Miron
 * @version 1
 */
public class DaciaLogan extends Car {

    public DaciaLogan(int year) {
        super(DaciaLogan.class.getSimpleName(), year);
    }

    @Override
    public void moveForward() {
        super.moveForward();
        System.out.println("fast");
    }
}
