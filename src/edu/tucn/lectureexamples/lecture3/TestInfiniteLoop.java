package edu.tucn.lectureexamples.lecture3;

/**
 * @author Radu Miron
 * @version 1
 */
public class TestInfiniteLoop {
    public static void main(String[] args) {
        Student student = new Student();
        System.out.println(student.toString());
        student.test();
        student.test(2);
        student.test("s");

        Person person = student;
        person.test();

        Person person1 = new Person(1);
//        Student student1 = (Student) person1; => ClassCastException which is an error

        Person person2 = new Student();
        Student student2 = (Student) person2;
        Student student3 = (Student) person;

    }
}

class Person { // superclass
    protected String name;

    Person(int i) { // (2)
//        new Student(); // this is the root cause of the endless loop
        System.out.println("Constructor of Person.");
    }

    public void test(){
        System.out.println("Person test");
    }
}

class Student extends Person { // subclass
//    private String name; // try to not hide an inherited attribute

    Student() { // (3)
        super(1);
        this.name = "Jim";
        super.name = "Tod";
        System.out.println("Constructor of Student.");
    }

    public void test(int i){
        System.out.println("Student test");
    }


    public void test(String s){
        System.out.println("Student test 2");
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + this.name + '\'' +
                ", name='" + super.name + '\'' +
                '}';
    }
}